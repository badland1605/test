var readline = require('readline'),
rl = readline.createInterface(process.stdin, process.stdout);
console.log('write numbers in words (E.g 1 = one, 2 = two)');
rl.on('line', function (userInput) {
    var newValue = userInput.replace(/[^a-zA-Z]/g, "");
    console.log('Total number of letters used: '+newValue.length);
});